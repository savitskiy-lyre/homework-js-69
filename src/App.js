import './App.css';
import React, {useEffect, useReducer} from "react";
import {initState, reducer} from "./store/reducer";
import {Route, useHistory} from "react-router-dom";
import axios from "axios";
import {calculateMovieIndex, fetchMovieInfo, fetchMoviesForSearching} from "./store/actions";
import MovieInfo from "./containers/MovieInfo/MovieInfo";
import SearchMovie from "./containers/SearchMovie/SearchMovie";

const App = () => {
    const [state, dispatch] = useReducer(reducer, initState);
    const history = useHistory();
    useEffect(() => {
        (async () => {
            if (state.searchInpValue) {
                try {
                    const {data} = await axios('http://api.tvmaze.com/search/shows?q=' + state.searchInpValue);
                    dispatch(fetchMoviesForSearching(data.map(movie => movie.show)));
                } catch (e) {
                    console.log(e, 'here');
                }
            }
        })();
    }, [state.searchInpValue]);
    useEffect(() => {
        if (!state.selectedSearchInpValue) {
            history.push('/');
        }
        if (state.selectedSearchInpValue) {
            dispatch(calculateMovieIndex());
        }
        return () => {
            dispatch(fetchMovieInfo(null));
        }
    }, [state.selectedSearchInpValue, history])
    useEffect(() => {
        if (state.selectedSearchMovieIndex)
            history.push('/shows/' + state.selectedSearchMovieIndex);
    }, [state.selectedSearchMovieIndex, history])

    return (
        <div className='App'>
            <Route path={'/'} render={() => (<SearchMovie state={state} dispatch={dispatch}/>)}/>
            <Route path={'/shows/:movieId'}
                   render={() => <MovieInfo dispatch={dispatch} movieInfo={state.movieInfo}/>}/>
        </div>
    );
};

export default App;
