export const FETCH_MOVIES_FOR_SEARCHING = 'FETCH_MOVIES_FOR_SEARCHING';
export const SEARCH_INP_VALUE = 'SEARCH_INP_VALUE';
export const SELECTED_SEARCH_INP_VALUE = 'SELECTED_SEARCH_INP_VALUE';
export const FETCH_MOVIE_INFO = 'FETCH_MOVIE_INFO';
export const CALCULATE_MOVIE_ID = 'CALCULATE_MOVIE_ID';


export const fetchMoviesForSearching = (movies) => {
   return ({type: FETCH_MOVIES_FOR_SEARCHING, payload: movies})
};
export const giveSearchInpValue = (value) => {
   return ({type: SEARCH_INP_VALUE, payload: value});
}
export const giveSelectedSearchInpValue = (value) => {
   return ({type: SELECTED_SEARCH_INP_VALUE, payload: value});
}
export const fetchMovieInfo = (data) => {
   return ({type: FETCH_MOVIE_INFO, payload: data});
}
export const calculateMovieIndex = () => {
   return ({type: CALCULATE_MOVIE_ID});
}