import {
    CALCULATE_MOVIE_ID,
    FETCH_MOVIE_INFO,
    FETCH_MOVIES_FOR_SEARCHING,
    SEARCH_INP_VALUE,
    SELECTED_SEARCH_INP_VALUE
} from "./actions";

export const initState = {
    moviesForSearching: [],
    selectedSearchInpValue: null,
    selectedSearchMovieIndex: null,
    searchInpValue: '',
    movieInfo: null,
};
export const reducer = (state, action) => {
    switch (action.type) {
        case FETCH_MOVIES_FOR_SEARCHING:
            if (action.payload === null) return {...state, moviesForSearching: initState.moviesForSearching}
            return {...state, moviesForSearching: action.payload};
        case SEARCH_INP_VALUE:
            return {...state, searchInpValue: action.payload};
        case SELECTED_SEARCH_INP_VALUE:
            return {...state, selectedSearchInpValue: action.payload};
        case FETCH_MOVIE_INFO:
            return {...state, movieInfo: action.payload}
        case CALCULATE_MOVIE_ID:
            if (state.moviesForSearching.length === 0) return state;
            const foundMovie = state.moviesForSearching.find(option => {
                return option.name === state.selectedSearchInpValue;
            })
            console.log(foundMovie);
            return {...state, selectedSearchMovieIndex: foundMovie ? foundMovie.id : state.moviesForSearching[0].id}
        default:
            return state;
    }
}