import React from 'react';
import {Autocomplete} from "@material-ui/lab";
import {giveSearchInpValue, giveSelectedSearchInpValue} from "../../store/actions";
import {TextField} from "@material-ui/core";

const SearchMovie = ({state, dispatch}) => {
    return (
        <div style={{maxWidth: 800, margin: '10px auto'}}>
            <Autocomplete
                freeSolo
                value={state.selectedSearchInpValue}
                onChange={(e, newValue) => {
                    dispatch(giveSelectedSearchInpValue(newValue));
                }}
                inputValue={state.searchInpValue}
                onInputChange={(event, newInputValue) => {
                    dispatch(giveSearchInpValue(newInputValue));
                }}
                options={state.moviesForSearching.length > 0 ? state.moviesForSearching.map((option) => {
                    return option.name;
                }) : state.moviesForSearching}
                renderInput={(params) => (
                    <TextField {...params} label="Search TV Show" margin="normal" variant="outlined"/>
                )}
            />
        </div>
    );
};

export default SearchMovie;