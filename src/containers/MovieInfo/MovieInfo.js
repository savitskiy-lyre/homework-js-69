import React, {useEffect} from 'react';
import {useRouteMatch} from "react-router-dom";
import axios from "axios";
import {fetchMovieInfo} from "../../store/actions";
import './MovieInfo.css';

const MovieInfo = ({movieInfo, dispatch}) => {
    const match = useRouteMatch();
    useEffect(() => {
        (async () => {
            if (match.params.movieId === 'null') return;
            try {
                const {data} = await axios.get('https://api.tvmaze.com/shows/' + match.params.movieId);
                dispatch(fetchMovieInfo(data));
            } catch (e) {
                console.log(e);
            }
        })();

    }, [match.params.movieId, dispatch]);
    return movieInfo ? (
        <div className='film-wrapper'>
            {movieInfo.image.medium && (
                <div>
                    <img src={movieInfo.image.medium} alt={'moviePic'}/>
                </div>
            )}
            <div style={{minWidth: 200}}>
                <div className='film-item'>
                    <strong>Film name:</strong> {movieInfo.name}
                </div>
                <div className='film-item'>
                    <strong>Average time:</strong> {movieInfo.averageRuntime}
                </div>
                <div className='film-item'>
                    <strong>Language: </strong> {movieInfo.language}
                </div>
                <div className='film-item'>
                    <div>
                        <strong>Genres:</strong>
                    </div>
                    <ul>
                        {movieInfo.genres.map((key) => <li key={movieInfo.id + key}>{key}</li>)}
                    </ul>
                </div>
            </div>
            <div>
                <div><strong>Description: </strong></div>
                <div dangerouslySetInnerHTML={{__html: movieInfo.summary}}/>
            </div>
        </div>
    ) : null;
};

export default MovieInfo;